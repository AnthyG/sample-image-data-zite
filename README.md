# Sample Image Data Zite

May be used in combination with a [Sample Image View Zite](https://gitlab.com/AnthyG/sample-image-view-zite).

Clone this repo into a new zite, edit the `ignore` property in it's `content.json` to look like follows:

`"ignore": "((js|css)/(?!(all|ZeroFrame).(js|css))|.git)"`

and the `optional` property:

`"optional": "(data/images/.*(png|jpg|jpeg))"`

Then copy the directory `data-default` to `data/` and create a sub-directory at `images` (so `data/images`).

Uploading is only possible when being zite admin.

The code located in the following files is copied from the respective file in https://github.com/HelloZeroNet/ZeroMe:

`js/Class.coffee` -> `js/lib/Class.coffee`
`js/ImagePreview.coffee` -> `js/utils/ImagePreview.coffee`
`js/Uploadable.coffee` -> `js/utils/Uploadable.coffee`