VERSION = 0.2;

datalist = null;
thumbnails = null;

dataURLtoFile = function(dataurl, filename) {
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
};

getThumbnails = function(cb) {
    page.cmd("fileGet", {
        "inner_path": "data/thumbnails.json",
        "required": true
    }, (_thumbnails) => {
        if (_thumbnails) {
            thumbnails = JSON.parse(_thumbnails);

            typeof cb === "function" && cb();
        }
    });
};

getDatalist = function(cb) {
    datalist = [];
    getThumbnails(function() {
        for (let name in thumbnails) {
            datalist.push(name);
        }

        typeof cb === "function" && cb(datalist);
    });
};

requestDatafileLoad = function(name, cb) {
    let filepath = "data/images/" + name;

    console.log("datafile load requesting", filepath);

    page.cmd("fileNeed", [filepath, 1000], (res) => {
        console.log("datafile load request", res, filepath);

        typeof cb === "function" && cb();
    });
};

requestDatafileDelete = function(name, cb) {
    let filepath = "data/images/" + name;

    console.log("datafile delete requesting", filepath);

    page.cmd("optionalFileDelete", [filepath], (res) => {
        console.log("datafile delete request", res, filepath);

        typeof cb === "function" && cb();
    });
};

checkDatafileInfo = function(name, img, btn_load, btn_delete) {
    page.cmd("optionalFileInfo", ["data/images/" + name], (res) => {
        if (!res) {
            res = {
                "is_downloaded": false
            };
        }

        console.log("got optional file info", name, res);

        img.src = res.is_downloaded ? "data/images/" + name : thumbnails[name];
        btn_load.style.display = !res.is_downloaded ? "" : "none";
        btn_delete.style.display = res.is_downloaded ? "" : "none";
    });
};

renderListItem = function(name) {
    let el = document.createElement("div");
    el.classList.add("list-item");

    el.appendChild(document.createTextNode(name));

    let img = document.createElement("img");
    img.src = thumbnails[name];
    el.appendChild(img);

    let btn_info = document.createElement("button");
    btn_info.innerHTML = "Get info";
    btn_info.onclick = function() {
        checkDatafileInfo(name, img, btn_load, btn_delete);
    };
    el.appendChild(btn_info);

    let btn_load = document.createElement("button");
    btn_load.innerHTML = "Load image";
    btn_load.onclick = function() {
        requestDatafileLoad(name, function() {
            checkDatafileInfo(name, img, btn_load, btn_delete);
        });
    };
    el.appendChild(btn_load);

    let btn_delete = document.createElement("button");
    btn_delete.innerHTML = "Delete image";
    btn_delete.onclick = function() {
        requestDatafileDelete(name, function() {
            checkDatafileInfo(name, img, btn_load, btn_delete);
        });
    };
    el.appendChild(btn_delete);

    checkDatafileInfo(name, img, btn_load, btn_delete);

    renderlist.appendChild(el);
};

renderList = function() {
    renderlist.innerHTML = "";

    for (let dliI = 0; dliI < datalist.length; dliI++) {
        let dli = datalist[dliI];
        renderListItem(dli);
    }
};

class Page extends ZeroFrame {
    setSiteInfo(site_info) {
        var out = document.getElementById("out");
        out.innerHTML =
            "Page address: " + site_info.address +
            "<br>- Zite Version: " + VERSION +
            "<br>- Peers: " + site_info.peers +
            "<br>- Size: " + site_info.settings.size +
            "<br>- Modified: " + (new Date(site_info.content.modified * 1000));
    }

    onOpenWebsocket() {
        this.cmd("siteInfo", [], function(site_info) {
            page.setSiteInfo(site_info);
        });


        page.upload = new Uploadable(page.handleUpload);
        page.upload.resize_width = 900;
        page.upload.resize_height = 700;

        page.image = new ImagePreview();

        console.log(page.upload);

        let btn = document.createElement("button");
        btn.appendChild(document.createTextNode("upload"));
        btn.onclick = page.upload.handleUploadClick;
        buttons.appendChild(btn);
    }

    handleUpload(base64uri, width, height) {
        page.image.base64uri = base64uri;
        page.image.width = width;
        page.image.height = height;
        let preview_width = 10;
        let preview_height = 10;
        page.upload.getPreviewData(base64uri, preview_width, preview_height, (preview_data) => {
            page.image.preview_data = preview_data;

            console.log(page.image);

            let imgp = document.createElement("img");
            imgp.src = page.image.getPreviewUri(preview_width, preview_height);

            uploadpreviews.appendChild(imgp);

            let filename = (new Date().valueOf()).toString();
            let filedata = dataURLtoFile(base64uri, filename);
            filename += "." + filedata.type.split("/")[1];

            console.log("turned uri into data", filename, filedata);

            let filereader = new FileReader();
            filereader.addEventListener("loadend", function() {
                // console.log("finished reading file", filereader.result);

                page.cmd("fileWrite", ["data/images/" + filename, btoa(filereader.result)],
                    (res) => {
                        console.log("result: ", res, filename);

                        let img = document.createElement("img");
                        img.src = "data/images/" + filename;

                        uploads.innerHTML = "";

                        uploads.appendChild(img);

                        page.cmd("fileGet", ["data/thumbnails.json"], (thumbnails) => {
                            if (thumbnails) {
                                thumbnails = JSON.parse(thumbnails);

                                thumbnails[filename] = page.image.getPreviewUri();

                                let json_raw = unescape(encodeURIComponent(JSON.stringify(thumbnails, undefined, '\t')));

                                page.cmd("fileWrite", ["data/thumbnails.json", btoa(json_raw)], (res2) => {
                                    console.log("updated thumbnails.json", res2);

                                    page.cmd("sitePublish", {
                                        "privatekey": "stored",
                                        "inner_path": "content.json",
                                        "sign": true
                                    }, (res3) => {
                                        console.log("site publish", res3);
                                    });
                                });
                            }
                        });
                    }
                );
            });
            filereader.readAsBinaryString(filedata);
        });
    }

    onRequest(cmd, message) {
        if (cmd == "setSiteInfo") {
            this.setSiteInfo(message.params);
        } else {
            this.log("Unknown incoming message:", cmd);
        }
    }
}
page = new Page();

btn_getDatalist = document.createElement("button");
btn_getDatalist.appendChild(document.createTextNode("Load"));
btn_getDatalist.onclick = function() {
    getDatalist(function() {
        renderList();
    });
};
buttons.appendChild(btn_getDatalist);